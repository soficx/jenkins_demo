def createJarpackage() {
    echo "Build the jar artifact...."
    sh "mvn package"
}

def createImage() {
    echo "Create a docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-cred', usernameVariable: 'USER', passwordVariable: 'TOKEN')]){
        sh "docker build -t soficx/maven_app:mvn-0.1 ."
        sh "docker login -u ${USER} -p ${TOKEN}"
        sh "docker push soficx/maven_app:mvn-0.1"
    } 
}

return this